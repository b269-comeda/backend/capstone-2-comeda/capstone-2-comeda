const User = require("../models/User");
const Product = require("../models/Product"); 
const bcrypt = require("bcrypt");
const auth = require("../auth");

// to register a user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			let successfulRegistrationMessage = `${reqBody.email} is now registered!`
			return successfulRegistrationMessage;
		};
	});
};

// log in for registered user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			let noEmailMessage = `Please input a registered email.`
			return noEmailMessage;
		} else if (reqBody.password == null) {
			let noPasswordIncluded = `Password required!`
			return noPasswordIncluded;
		} else {
			const isPasswordRight = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordRight) {
				return {access: auth.createAccessToken(result)};
			} else {
				let errorMessage = `Password is incorrect!`
				return errorMessage;
			};
		};
	});
};




