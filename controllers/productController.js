const Product = require("../models/Product");

// creating product (ADMIN ONLY)
module.exports.createProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product ({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price 
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				let productCreatedMessage = `Product successfully added to the database!`
				return productCreatedMessage;
			};
		});
	};
	let notAdminMessage = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return notAdminMessage.then((value) => {
		return {value};
	});
	
};

// retrieving all ACTIVE products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};



