const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://jeremiahcomeda:admin123@zuitt-bootcamp.q8eipm4.mongodb.net/capstone2comeda?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log('Now connected to the database!'));

app.use("/users", userRoute);
app.use("/products", productRoute);

app.listen(process.env.PORT || 3000, () => console.log(`Now connected to port ${process.env.PORT || 3000}`));